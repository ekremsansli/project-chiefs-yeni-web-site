<?php
/**
 * WordPress için taban ayar dosyası.
 *
 * Bu dosya şu ayarları içerir: MySQL ayarları, tablo öneki,
 * gizli anahtaralr ve ABSPATH. Daha fazla bilgi için 
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php düzenleme}
 * yardım sayfasına göz atabilirsiniz. MySQL ayarlarınızı servis sağlayıcınızdan edinebilirsiniz.
 *
 * Bu dosya kurulum sırasında wp-config.php dosyasının oluşturulabilmesi için
 * kullanılır. İsterseniz bu dosyayı kopyalayıp, ismini "wp-config.php" olarak değiştirip,
 * değerleri girerek de kullanabilirsiniz.
 *
 * @package WordPress
 */

// ** MySQL ayarları - Bu bilgileri sunucunuzdan alabilirsiniz ** //
/** WordPress için kullanılacak veritabanının adı */
define('DB_NAME', 'pj_new');

/** MySQL veritabanı kullanıcısı */
define('DB_USER', 'root');

/** MySQL veritabanı parolası */
define('DB_PASSWORD', 'root');

/** MySQL sunucusu */
define('DB_HOST', 'localhost');

/** Yaratılacak tablolar için veritabanı karakter seti. */
define('DB_CHARSET', 'utf8mb4');

/** Veritabanı karşılaştırma tipi. Herhangi bir şüpheniz varsa bu değeri değiştirmeyin. */
define('DB_COLLATE', '');

/**#@+
 * Eşsiz doğrulama anahtarları.
 *
 * Her anahtar farklı bir karakter kümesi olmalı!
 * {@link http://api.wordpress.org/secret-key/1.1/salt WordPress.org secret-key service} servisini kullanarak yaratabilirsiniz.
 * Çerezleri geçersiz kılmak için istediğiniz zaman bu değerleri değiştirebilirsiniz. Bu tüm kullanıcıların tekrar giriş yapmasını gerektirecektir.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^GXb|c`Al|[yHp6!nZfT6MIMDGIE^**=jz+=Nx?^`4KUGBVtiC7o(zLVQ>UtN1D%');
define('SECURE_AUTH_KEY',  'Y1-/h4#05@cgI[X.;C@fa:497>MO/5Qb#Qmken8Vu)kn3/r4ae}H:m^&cf*/oh*^');
define('LOGGED_IN_KEY',    'wW p~/5KC$Lvs12,X QR|iSrd?U:FnW|2.vmva_eDaY| Q8;(qFa&9wi@g9Jh^qG');
define('NONCE_KEY',        '9QJrIxZH)^)Xlf=`JE>nHk(Np?ydZ_%J|OyYRxw*z -gYGSL/::A_A8jtoA#{&(5');
define('AUTH_SALT',        '[5Ytt.KNtFdG${&.)Zg^og35M~]o>,(8l:w|Ym?Os.G`2g:uVhMkC(59HMNuue^T');
define('SECURE_AUTH_SALT', 'kpb>M=KyjnS)dHbLV).C=rvA2Xv18G(]4n/8aV/LPp3>Rl2tv<h=%/yW{*YjKb`6');
define('LOGGED_IN_SALT',   ' lf^3=gb7%Fyc$`|KQ%= vNOuJIAUbt3NAN_UW6h=I/c@p3h}i6d6O&clKl^[^5&');
define('NONCE_SALT',       '#=~|2Cz$LYe<YbcZdCs4:st9uHH}DY{K}<jhVCaAo wPQZ%{$;~7Uz-({l<]=M;}');
/**#@-*/

/**
 * WordPress veritabanı tablo ön eki.
 *
 * Tüm kurulumlara ayrı bir önek vererek bir veritabanına birden fazla kurulum yapabilirsiniz.
 * Sadece rakamlar, harfler ve alt çizgi lütfen.
 */
$table_prefix  = 'wp_';

/**
 * Geliştiriciler için: WordPress hata ayıklama modu.
 *
 * Bu değeri "true" yaparak geliştirme sırasında hataların ekrana basılmasını sağlayabilirsiniz.
 * Tema ve eklenti geliştiricilerinin geliştirme aşamasında WP_DEBUG
 * kullanmalarını önemle tavsiye ederiz.
 */
define('WP_DEBUG', false);

/* Hepsi bu kadar. Mutlu bloglamalar! */

/** WordPress dizini için mutlak yol. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** WordPress değişkenlerini ve yollarını kurar. */
require_once(ABSPATH . 'wp-settings.php');
